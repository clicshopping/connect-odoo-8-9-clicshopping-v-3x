# -*- encoding: utf-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    This module copyright (C) 2014 e-imaginis
#    (http://www.clicshopping.org).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models
from openerp.tools.translate import _

class product_template(models.Model):
    _inherit = "product.template"
    _description = "ClicShopping Product Template"

# Product
    clicshopping_products_id = fields.Integer(string="Product Id", size=5, help="Id product table of ClicShopping must be unique"),
    ClicShopping_products_save_to_catalog = fields.Boolean(string="ClicShopping", help="Save the product inside ClicShopping"), 
    clicshopping_products_sku = fields.Char(string="SKU", size=10, help="SKU for products must be unique"),
    clicshopping_products_weight_pounds = fields.Float(string="Product Weight Pounds", size=5, help="Weight pounds Product table of ClicShopping"),
    clicshopping_products_dimension_width = fields.Float(string="Width Dimension", size=5, help="Width Dimension Product table of ClicShopping"),
    clicshopping_products_dimension_height = fields.Float(string="Height Dimension", size=5, help="Height Dimension Product table of ClicShopping"),
    clicshopping_products_dimension_depth = fields.Float(string="Depth Dimension", size=5, help="Depth Dimension Product table of ClicShopping"),
    clicshopping_products_dimension_depth = fields.Float(string="Depth Dimension", size=5, help="Depth Dimension Product table of ClicShopping"),
    clicshopping_products_dimension_type = fields.Char(string="Dimension type", size=5, help="mn, cm, dm, m .... Product table of ClicShopping"),
#Other
    clicshopping_products_price_comparison = fields.Boolean(string="Price Comparator", help="Exlude the product of price comparator exportation. Not active by default"),
    clicshopping_products_only_online = fields.Boolean(string="Product Only Online",  help="If the product is only sell Online"),
    clicshopping_products_only_shop = fields.Boolean(string="Product Only Shop", help="If the product is only sell in the shop"),

    clicshopping_products_packaging = fields.Selection([('New product','New product packaging'),
                                                        ('Product repackaged','Product repackaged'),
                                                        ('Product used','Product used')],
                                                        'Packaging type', required=0),

    clicshopping_products_description = fields.Text(string="HTML Description", translate=True,  help="Html ClicShopping content"),
#stock
    clicshopping_products_stock_status = fields.Boolean(string="Stock Status",  default='1', help="If a product is not active (out of stock, it will not be displayed in the catalog - See also the defautl configuration on ClicShopping"),
    clicshopping_products_quantity_alert = fields.Float(string="Alert  Stock", size=5, help="Specify your alert Stock on ClicShopping. Let empty for default stock defined in ClicShopping"),
    clicshopping_products_min_qty_order = fields.Float(string="Product Min Quantity Order",  default='0', size=5, help="Specify your minimum Quantity order on ClicShopping. Let empty for default Quantity. See ClicShopping for more information"),
    clicshopping_products_date_available = fields.Char(string="Available Date", help="Available date of products"),
#price
    clicshopping_products_price_kilo = fields.Boolean(string="Display Price by kilo", help="Display on the store the price by kilo"),
    clicshopping_products_view = fields.Boolean(string="Product View", default='1', help="Display in B2C the product"),
    clicshopping_products_handling = fields.Float(string="Cost of handling", size=5, help="Delay of handling"),
    clicshopping_products_orders_view = fields.Boolean(string="Product Order View", default='1', help="Display in B2C the purchase button"),
#User
    clicshopping_admin_user_name = fields.Char(string="User Name", help="Who has created this product"),
#B2B
    clicshopping_products_percentage = fields.Boolean(string="Customer Price Group Calcul", default='1', help="Automatic or Manuel price calcul for B2B. Automatic, the percentage (discount or increased) is calculted in function the customer group else if manual, the calcul is not automated. See the default parameters in ClicShopping and customers group"),
#customers group apply on product    
    clicshopping_products_group_id_products = fields.One2many('clicshopping.products.group', 'clicshopping_products_id', 'Products Group'),



class clicshopping_products_group(models.Model):
    _inherit = 'clicshopping.products.group'
    _description = "Information about a product group"

    sequence = fields.Integer(string="Sequence", help="Assigns the priority to the list of product groups."),
    clicshopping_products_id = fields.Integer(string="Product Id", size=5, help="Id product must be unique"),

#   clicshopping_customers_group_id = fields.Many2one('clicshopping.customers.group', 'Customer group Id', help='id of customers group'), 
    clicshopping_customers_group_id = fields.Integer(string="Customer group Id", size=20, help='id of customers group'),
    clicshopping_products_model_group = fields.Char(string="Product model group", size=30, help='Model'),



#   clicshopping_price_group_view = fields.Boolean(string="Price group view", default='1', help='Display Price Group View'),
#   clicshopping_products_group_view = fields.Boolean(string="Product Group View", default='1', help='Display Group View'),
#   clicshopping_orders_group_view = fields.Boolean(string="Display Order process", default='1', help='Display order Group View'),

    _order = 'sequence'

    _defaults = {
       sequence : 1,
    }

