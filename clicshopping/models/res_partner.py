# -*- encoding: utf-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    This module copyright (C) 2014 e-imaginis
#    (http://www.clicshopping.org).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models
from openerp.tools.translate import _

class res_partner(models.Model):
    _inherit = 'res.partner'
    _description = "ClicShopping partner"

    clicshopping_customers_id = fields.Integer(string="Customer Id", size=5, help="Id of customer of ClicShopping must be unique"),
    ClicShopping_customer_save_to_catalog = fields.boolean(string="ClicShopping", help="Save the customer inside ClicShopping"), 
    clicshopping_customer_discount = fields.Float(string="Customer Discount", size=5, help="Specific discount for the customer"),
    clicshopping_suppliers_id = fields.Integer(string="Supplier Id", size=5, help="Id of supplier table of ClicShopping must be unique"),
    clicshopping_suppliers_notes = fields.Text(string="Supplier Note", size=5, help="Note on the supplier"),
    clicshopping_customers_additional_address_id = fields.Integer(string="additionnal address Id", size=5, help="Id of customer when it create a new address inside ClicShopping"),

