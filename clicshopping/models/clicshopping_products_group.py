# -*- encoding: utf-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    This module copyright (C) 2014 e-imaginis
#    (http://www.clicshopping.org).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

#access_product_clicshopping_products_group_user,product.clicshopping_products_group.user,model_product_clicshopping_products_group,base.group_user,1,0,0,0

from openerp import models, fields
from openerp.tools.translate import _

class clicshopping_products_group(models.Model):
    _name = 'clicshopping.products.group'

    clicshopping_customers_group_id = fields.Integer(string="Customer group Id", size=20, help="id of customers group"),
    clicshopping_customers_group_price = fields.Float(string="Customers group Price", size=70, help="Price of the product group"),
    clicshopping_products_id = fields.Integer(string="Product Id", size=5, help="Id product must be unique"),
    clicshopping_products_price = fields.Float(string="Products_price",size=70, help="price of the product"),
    clicshopping_price_group_view = fields.Boolean(string="Price group view", default='1', help="Display Price Group View"),
    clicshopping_products_group_view = fields.Boolean(string="Product Group View", default='1', help="Display Group View"),
    clicshopping_orders_group_view = fields.Boolean(string="Display Order process", default='1', help="Display order Group View"),
    clicshopping_products_model_group = fields.Char(string="Product model group", size=30, help="Product model"),
    clicshopping_products_quantity_unit_id_group = fields.Integer(string="product Quantity Unit group", help="Default quantity for this group"),
    clicshopping_products_quantity_fixed_group = fields.Integer(string="product quanty Fixed", help="Default quantity for this group"),

