# -*- encoding: utf-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    This module copyright (C) 2014 Innov'Concept Consulting
#    (http://www.clicshopping.org).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'ClicShopping',
    'version': '0.1',
    'maintainer': 'Innov'Concept Consulting',
    'license': 'GPL-2',
    'summary': 'Connect ClicShopping and Odoo',
      
    'description': """

This module connects Odoo and ClicShopping.

ClicShopping(TM) (http://www.clicshopping.org/) is a popular social e-commerce B2B/B2C plateform Open Source written in PHP/MySQL and published under the Open Software licence v2.0.
He has been developped by Innov'Concept Consulting (http://www.innov-concept.com)

ClicShopping integrate natively via webservice a connexion in XML-RPC with Odoo to export the features in real time or manual.

- Customers (create / update)
- Customers Groups (create / update)
- Products (create / update)
- Products groups (create / update)
- Product categories (create / update)
- Suppliers (create / update)
- Orders (create /update)
- Invoice (create / update)
- Wharehouse (create)
- Stock (create / update)
- Brands (create / update)
- Claim 
- Mass mailing

This module allows you to create a synchronisation Odoo to ClicShopping and export inside ClicShopping the following features in realtime.
It also take the difference between B2B and B2C (not completly available at this time)

- Customers
- Customers groups (notcomplete)
- products
- Suppliers
- Orders
- invoice
- Claim
- Stock
- Categories
- Product categories
- Brands

If you have question, please push on Odoo community
Lot of videos are available on youtube, search ClicShopping

Thank you for Odoo Community for this help.


Loïc Richard 
(loic.richard@innov-concept.com)

If you  want donate something for this work, you can via Paypal : commande@clicshopping.com


How to use this webservice

ODOO
Add Odoo Dependance general configuration

- Menu Configuration / Sales
   Activate : Autorize another address for the shipping and Order address must be selected
   Activate : Manage the customers claims and Manage the assistance and support must be selected

CLICSHOPPING

- Menu Configuration / Administration / Odoo Webservice -> fill and follow all the steps

   If need to update the invoice since administration, For all instance order, you must update the order to activate the export in Odoo
   If you don't register by the catalog the order, please update the instance order to axport the order in Odoo

   In Catalog, if you use invoice export system,the shipping and order address is not write in Odoo (no option in invoice in Odoo to manage this). Just the defautl customer address is writen.
   Accounting, Update the number account in function your Odoo accounting (you must maybe create account in Odoo if something does'nt exist)



""",

    'author': 'Innov'Concept Consulting',
    'images': ['images/icon.png'],
    'website': "https://www.clicshopping.org",
    'category': "Features",

# any module necessary for this one to work correctly
    'depends': [
                'account_accountant',
                'account',
                'account_voucher',
                'sale',
                'product',
                'stock',
#                'document',
                'crm_claim',
                'claim_from_delivery',
                'mass_mailing',
        ],


    'external_dependencies': {
        'python': [],
    },
    
# always loaded
    'data': [
            'security/security.xml',
             'security/ir.model.access.csv',
             'views/product_clicshopping_view.xml',
             'views/res_partner.xml',
             'views/clicshopping_manufacturer_view.xml',
             'views/product_category_view.xml',
             'views/clicshopping_invoice_view.xml',
             'views/clicshopping_sale_order_view.xml',
             'views/clicshopping_config.xml',
             'views/clicshopping_customers_group.xml',
             'views/clicshopping_products_group.xml',
        ],

# only loaded in demonstration mode
    'demo': [
        ],
    
# used for Javascript Web CLient Testing with QUnit / PhantomJS
# https://www.odoo.com/documentation/8.0/reference/javascript.html#testing-in-odoo-web-client
    'js': [],
    'css': [],
    'qweb': [],

    'installable': True,
# Install this module automatically if all dependency have been previously and independently installed.
# Used for synergetic or glue modules.
    'auto_install': False,
    'application': True,
}
