# README #

ClicShopping_v3x_Odoo_8_9

Addon to connect ClicShopping and Odoo - In development

1 - To install, you must insert in Odoo addons directory 
2 - Update module List
3 - Install

Information inside Odoo

- Customers
- Customers groups
- Products
- Products Groups
- Wharehouse
- Sales
- Invoices
- Suppliers
- Brands / Manufacturers
- Claim / Support (not include at this time)

Dependency :

Sale
Account
Account Voucher
Accounting
product
stock



Development :
1st step : Relation ClicShopping to odoo (done)
2nd step : Relation Odoo to ClicShopping (developement)

For more information concerning the configuration, see clicshopping.org website

